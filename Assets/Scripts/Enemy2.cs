﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour {

    public Vector2 moveSpeed;

    public void LaunchEnemy(Vector3 position, Vector2 direction, float rotation)
    {
        transform.position = position;
        moveSpeed = direction;
    }


    void Update ()
    {
        transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(this.gameObject);
    }
}
